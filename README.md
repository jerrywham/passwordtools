# PasswordTools : a class allowing the management of passwords.

## Class Features
- create hash for password using the last and recommanded function password_hash from php
- validate password
- rehash password if an old version of function have been used in a project
- verify if a password is strong enough
- verify if a password has been powned using pwnedpasswords.com api

## Requirements

For php 7.0+.

## Installation & loading

Copy the contents of the passwordTools folder into one of the `include_path` directories specified in your PHP configuration and load each class file manually:

```php
<?php
// Import PasswordTools classes into the global namespace
// These must be at the top of your script, not inside a function
use jerrywham\passwordtools;

require 'path/to/passwordTools/class.passwordtools.php';
```

In your project, you need to include the class file `passwordtools.php` or, if you use an autoloader, you can use the namespace `jerrywham\passwordtools`

## Licence
This software is distributed under the [GNU AGPLv3](https://choosealicense.com/licenses/agpl-3.0/) license. Please read LICENSE for information on the software availability and distribution.

## A Simple Example

An example of a non-functional class as it stands but which gives an overview of how to use the tool. To be adapted according to the project in which it will be used

```php
<?php
// Import PasswordTools classes into the global namespace
// These must be at the top of your script, not inside a function
use jerrywham\passwordtools;

require __DIR__.DIRECTORY_SEPARATOR.'class.passwordtools.php';

class myClass {

    // passwordTools object
    private $PW;
    // minimal lenght of passwords to be strong
    private $pwLen = 12;

    public function __construct() {
        $this->PW = new passwordtools();
    }

    public function defPassword($password) {
        return $this->PW->create_hash($password);
    }
    
    public function isPasswordLeaked($password) {
        return $this->PW->isPasswordHaveBeenPowned($password);
    }

    public function editMyAccount($compte,$password) {
        if (true === $this->PW->isPasswordStrong($compte['password'], $this->pwLen) ) {
            $compte->password = $this->PW->create_hash($password);
        }
        $this->record($compte);
    }

    public function verifPass($pw,$login){
        $user = $this->getUser($login);
        if (true === $this->PW->validate_password($pw,$user['pass'])) {
            unset($_SESSION['maxtryAdh'],$_SESSION['timeoutAdh']);
            return true;
        }
        return false;
    }

    public function getUser($login)
    {
        // To be write 
        return $user;
    }
}
