<?php 

namespace jerrywham\interfaces;

interface passwordtools {

    /**
     * Constructor of the class
     * @param bool|boolean $weak                   [if password can be weak. For test only.]
     * @param bool|boolean $pownedPasswordDatabase [True if you want to use pwnedpasswords.com api]
     */
    public function __construct(bool $weak=false, bool $pownedPasswordDatabase=true);
    
    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value);
    public function __call($name, $arguments);
    public function __isset($name);
    public function __unset($name);
    public function __get($var);

    /**
     * Note that the salt here is randomly generated.
     * Never use a static salt or one that is not randomly generated.
     *
     * For the VAST majority of use-cases, let password_hash generate the salt randomly for you
     * @param string $password [the password to hash]
     * @return string           [the hash]
     */
    public function create_hash(string $password):string;

    /**
     * Validate the given password compare to good hash in base
     * @param  string $password  [the password to validate]
     * @param  string $good_hash [the hash of the initial password]
     * @return [bool]            [True if password is good. False otherwise]
     */
    public function validate_password(string $password, string $good_hash):bool;

    /**
     * If hash is old, the given password is re-hashed. This function is used for password hashed with old password_hash function
     * @param  string  $password [the given password]
     * @param  string  $hash     [the hash in base]
     * @return mixed           [the new hash or false]
     */
    public function isPasswordNeedsRehash(string $password, string $hash);

    /**
     * Use pwnedpasswords.com api to know if the password have been powned
     * @param  string  $pw [description]
     * @return bool|boolean     [True if the password have been powned. False otherwise or if api is unavailable]
     */
    public function isPasswordHaveBeenPowned(string $pw):bool;

    /**
     * Check is the password given to the function is strong enough
     * @param  string  $pw      [the password in clear before hash]
     * @param  int     $length  [the minimum of lenght to be accepted]
     * @param  string  $compare [the last password used. This param should be used when password needs to be changed. Application have to ask the old password, hash it to be sure it is the good one and compare it to the new one to be sure is different enough]
     * @return bool|boolean          [True if is strong enough. False otherwise]
     */
    public function isPasswordStrong(string $pw,int $length,string $compare=''):bool;

}